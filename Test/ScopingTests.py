from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Integer
from AST import DefaultScope

tests.clear()

@Test
def block_test():
    tokens = Lexer('{a = 1;} {a=2;{a = 3;}} {func test() {} {{}}}').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 3

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert 'a' not in scope.objects and 'test' not in scope.objects

@Test
def access_test():
    tokens = Lexer('''
    global = 1;
    {
        global = 2;
        private = "";
    }
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('global') == Integer(2)
    assert 'private' not in scope.objects

@Test
def for_test():
    tokens = Lexer('''
        global = 2;
        for i = 0; i < 10; i = i + 1; {
            a = i;
        }
    ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('global') == Integer(2)
    assert 'i' not in scope.objects
    assert 'a' not in scope.objects