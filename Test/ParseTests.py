from Exception import ParseException, InvalidTypeException
from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from AST import BlockNode, DefaultScope
from Type import Integer, String, Null

tests.clear()

@Test
def assignment_test():
    tokens = Lexer('a = 1+2;Test_Identifier       =    2^3^4; String = "String"; b = a + 1;').tokenize()
    AST = Parser(tokens).parse()

    assert type(AST) == BlockNode

    scope = DefaultScope()

    for node in AST.statements:
        node.evaluate(scope)

    values = {
        'a': Integer(3),
        'Test_Identifier': Integer(2**3**4),
        'String': String('String'),
        'b': Integer(4)
    }

    for variable, value in values.items():
        assert scope.get_object(variable) == value

@Test
def illegal_assignment_test():
    exception = None

    try:
        Parser(Lexer('a = b = 1;').tokenize()).parse()
    except ParseException as e:
        exception = e

    assert type(exception) == ParseException

@Test
def function_test():
    tokens = Lexer('func test(a,b,c)     {1+1;2;}').tokenize()
    AST = Parser(tokens).parse()
    assert len(AST.statements) == 1

    result = 'func test(a, b, c) {\n\t(1 + 1);\n\t2;\n}'
    assert str(AST.statements[0]) == result

@Test
def function_without_arguments():
    tokens = Lexer('func test_function() {2^3;}').tokenize()
    AST = Parser(tokens).parse()
    assert len(AST.statements) == 1

    result = 'func test_function() {\n\t(2 ^ 3);\n}'
    assert str(AST.statements[0]) == result

@Test
def function_assignment():
    tokens = Lexer('''
    a = func() {
        return 1;
    };
    b = a();
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('b') == Integer(1)

@Test
def global_variable_test():
    tokens = Lexer('''
    global = "test";
    func alter() {
        global = global + 1;
    }
    alter();
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('global') == String('test1')


@Test
def nested_function():
    tokens = Lexer('''
    func func1() {
        return func () {
            return func () {return "does it work?";}
        }
    }

    a = func1();
    b = a();
    c = b();
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('c') == String("does it work?")

@Test
def function_call_with_arguments():
    tokens = Lexer('test(1, 1+2, a);').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 1

    result = 'test(1, (1 + 2), a)'

    assert str(AST.statements[0]) == result

@Test
def function_return_function():
    tokens = Lexer('''
    func r() {
        return func() {
            return 1;
        };
    }
    a = r();
    b = a();
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('b') == Integer(1)

@Test
def function_call_without_arguments():
    tokens = Lexer('test2();').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 1

    result = 'test2()'

    assert str(AST.statements[0]) == result

@Test
def function_call_addition():
    tokens = Lexer('(t1() \t\n+    \tt2(\n)) + 2;').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 1

    result = '((t1() + t2()) + 2)'

    assert str(AST.statements[0]) == result

@Test
def function_call_exponentiation():
    tokens = Lexer('t()^z()  * s(r^t());').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 1

    result = '((t() ^ z()) * s((r ^ t())))'

    assert str(AST.statements[0]) == result

@Test
def function_call():
    tokens = Lexer('func test(){return 1;} test();').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 2

    scope = DefaultScope()
    AST.statements[0].evaluate(scope)

    assert AST.statements[1].evaluate(scope) == Integer(1)

@Test
def nested_function_call():
    tokens = Lexer('''
    func a(c) {
        return c + 1;
    }
    func b(c) {
        return a(c + 2);
    }
    result = b(0);
''').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 3
    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    var = scope.get_object('result')
    assert var == Integer(3)

@Test
def null_test():
    tokens = Lexer('a = null;').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 1
    scope = DefaultScope()
    AST.statements[0].evaluate(scope)

    assert type(scope.get_object('a')) == Null

@Test
def illegal_null_test():

    exception = None
    try:
        tokens = Lexer('null = 2;').tokenize()
        AST = Parser(tokens).parse()
    except ParseException as e:
        exception = e

    assert type(exception) == ParseException

    exception = None
    try:
        tokens = Lexer('null + 2;').tokenize()
        AST = Parser(tokens).parse()
        AST.statements[0].evaluate(DefaultScope())
    except InvalidTypeException as e:
        exception = e

    assert type(exception) == InvalidTypeException

