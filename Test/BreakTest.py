from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Integer
from AST import DefaultScope
from Exception import ParseException

tests.clear()

@Test
def basic_break_test():
    tokens = Lexer('''
            a = 0;
            while true {
                a = a+1;
                if a == 10 {
                    break;
                }
            }

            b = 2;
            for i = 0; i < 10; i = i + 1; {
                while true break;
                b = b * 2;
            }
        ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Integer(10)
    assert scope.get_object('b') == Integer(2048)

@Test
def wrong_break_usage():
    tokens = Lexer('''
                break;
            ''').tokenize()

    exception = None
    try:
        Parser(tokens).parse()
    except ParseException as e:
        exception = e

    assert type(exception) == ParseException

