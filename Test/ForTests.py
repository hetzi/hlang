from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Boolean, Integer
from AST import DefaultScope

tests.clear()

@Test
def basic_for_test():
    tokens = Lexer('''
        a = 0;
        for i = 0; i < 10; i=i+1;
            a = a+1;
    ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Integer(10)

@Test
def reversed_for_test():
    tokens = Lexer('''
            a = 0;
            for i = 10; i > 0; i=i-1;
                a = a+1;
        ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Integer(10)

@Test
def nested_for_loops():
    tokens = Lexer('''
            a = 0;
            for i = 0; i < 10; i=i+1;
                for j = 0; j < 10; j=j+1;
                    a = a + i * j;
        ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Integer(2025)

@Test
def for_as_while_loop():
    tokens = Lexer('''
        a = 0;
        for ; a < 10; ;
            a = a + 1;
    ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Integer(10)