from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Boolean, Integer
from AST import DefaultScope
from Exception import InvalidTypeException

tests.clear()

@Test
def bitwise_not():
    tokens = Lexer('~1; ~0; ~(~100); ~~1; -~-~20; -----1;').tokenize()
    AST = Parser(tokens).parse()

    results = [Integer(~1), Integer(~0), Integer(100), Integer(1), Integer(22), Integer(-1)]

    assert [node.evaluate(DefaultScope()) for node in AST.statements] == results

@Test
def bitwise_binary():
    tokens = Lexer('2348234 & 1231238 $ 234022348 | 23948345;').tokenize()
    AST = Parser(tokens).parse()

    result = Integer(234712447)

    assert len(AST.statements) == 1
    assert AST.statements[0].evaluate(DefaultScope()) == result

@Test
def bitshift():
    tokens = Lexer('34 >> 2; 234 << 23; 34 >> 2; 34 << 2 >> 2; 34 >> 2 << 2;').tokenize()
    AST = Parser(tokens).parse()

    results = [Integer(34 >> 2), Integer(234 << 23), Integer(34 >> 2), Integer(34), Integer(32)]

    assert [node.evaluate(DefaultScope()) for node in AST.statements] == results

@Test
def invalid_bitshift():

    invalid = ['34 >> "str";', 'true << 1;', 'true >> false;', '"s" >> 23;']

    for test in invalid:
        exception = None
        try:
            tokens = Lexer(test).tokenize()
            AST = Parser(tokens).parse()
            for node in AST.statements:
                node.evaluate(DefaultScope())
        except InvalidTypeException as e:
            exception = e

        assert type(exception) == InvalidTypeException

@Test
def equality():
    tokens = Lexer('''
    a = 1 == 1 == true;
    b = 2 == 1 + 1 || 3 == 2 + 2;
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Boolean(True)
    assert scope.get_object('b') == Boolean(True)

@Test
def logical_binary():
    tokens = Lexer('''
    f = true && false;
    t = true || false;

    a = !f;
    b = !!f;

    c = (!t && (true == true == !false == !!!false));
    d = !!t;

    x = 1 == 1;
    y = "test" == "test";
    z = true == true == true;
''').tokenize()
    AST = Parser(tokens).parse()

    assert len(AST.statements) == 9

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('f') == Boolean(False)
    assert scope.get_object('t') == Boolean(True)
    assert scope.get_object('a') == Boolean(True)
    assert scope.get_object('b') == Boolean(False)
    assert scope.get_object('c') == Boolean(False)
    assert scope.get_object('d') == Boolean(True)
    assert scope.get_object('x') == Boolean(True)
    assert scope.get_object('y') == Boolean(True)
    assert scope.get_object('z') == Boolean(True)

@Test
def equals_function():
    tokens = Lexer('''
    func equals(a, b) {
        return a == b;
    };
    t1 = equals(1, 1);
    t2 = equals("test", "test");
    t3 = equals(2^3, 2*2*2);

    f1 = equals(2, 1);
    f2 = equals("test", "test2");
    f3 = equals(1+1+1, false);
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('t1') == Boolean(True)
    assert scope.get_object('t2') == Boolean(True)
    assert scope.get_object('t3') == Boolean(True)

    assert scope.get_object('f1') == Boolean(False)
    assert scope.get_object('f2') == Boolean(False)
    assert scope.get_object('f3') == Boolean(False)

@Test
def not_equals_function():
    tokens = Lexer('''
    func not_equal(a, b) {
        return a != b;
    }

    t1 = not_equal(1, 2);
    t2 = not_equal("t", 1);
    t3 = not_equal(1.5, 1.6);

    f1 = not_equal(1, 1);
    f2 = not_equal(1.34, 1.34);
    f3 = not_equal("t\\"", "t\\"");
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('t1') == Boolean(True)
    assert scope.get_object('t2') == Boolean(True)
    assert scope.get_object('t3') == Boolean(True)

    assert scope.get_object('f1') == Boolean(False)
    assert scope.get_object('f2') == Boolean(False)
    assert scope.get_object('f3') == Boolean(False)

@Test
def greater():
    tokens = Lexer('2 > 1; 100.5 > 100; 4 > 4;').tokenize()
    AST = Parser(tokens).parse()

    results = [Boolean(True), Boolean(True), Boolean(False)]

    assert [node.evaluate(DefaultScope()) for node in AST.statements] == results

@Test
def greater_equals():
    tokens = Lexer('2 >= 1; 100.5 >= 100.5; 4 >= 4.001;').tokenize()
    AST = Parser(tokens).parse()

    results = [Boolean(True), Boolean(True), Boolean(False)]

    assert [node.evaluate(DefaultScope()) for node in AST.statements] == results

@Test
def smaller():
    tokens = Lexer('1 < 100; 1 < 1; 1 < 0.9999;').tokenize()
    AST = Parser(tokens).parse()

    results = [Boolean(True), Boolean(False), Boolean(False)]

    assert [node.evaluate(DefaultScope()) for node in AST.statements] == results

@Test
def smaller_equals():
    tokens = Lexer('1 <= 1; 10.5 <= 10; 10 <= 10.5;').tokenize()
    AST = Parser(tokens).parse()

    results = [Boolean(True), Boolean(False), Boolean(True)]

    assert [node.evaluate(DefaultScope()) for node in AST.statements] == results