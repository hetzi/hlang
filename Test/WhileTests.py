from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Boolean, Integer
from AST import DefaultScope

tests.clear()

@Test
def counting_loop():
    tokens = Lexer('''
    a = 0;
    while a != 100 {
        a = a + 1;
    }
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('a') == Integer(100)

@Test
def prime_counting():
    tokens = Lexer('''

    func is_prime(n) {
        if n < 2
            return false;
        if n == 2
            return true;
        if n % 2 == 0
            return false;
        counter = 3;
        while counter < n {
            if n % counter == 0
                return false;
            counter = counter + 1;
        }
        return true;
    }

    func count_primes(num) {
        prime_count = 0;
        c = 2;
        while c <= num {
            if is_prime(c)
                prime_count = prime_count + 1;
           c = c + 1;
        }
        return prime_count;
    }

    primes_to_10 = count_primes(10);
    primes_to_100 = count_primes(100);
    //primes_to_1000 = count_primes(1000);

''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('primes_to_10') == Integer(4)
    assert scope.get_object('primes_to_100') == Integer(25)
    #assert scope.get_object('primes_to_1000') == Integer(168)