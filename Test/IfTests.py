from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Integer
from AST import DefaultScope

tests.clear()

@Test
def basic_if_test():
    tokens = Lexer('''
    animal = "lion";
    result = 0;
    if animal == "tiger" || animal == "lion" {
        result = 1;
    } else if animal == "bird" {
        result = 2;
    } else {
        result = 3;
    }
''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('result') == Integer(1)

@Test
def if_else_chaining():
    tokens = Lexer('''

    animal = "fish";
    result = 0;

    test = func() {
        if animal == "tiger" || animal == "lion" {
            result = 1;
        } else if animal == "bird" {
            result = 2;
        } else {
            result = 3;
        }
    };

    test();
    result1 = result;

    animal = "tiger";
    test();
    result2 = result;

    animal = "bird";
    test();

''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('result1') == Integer(3)
    assert scope.get_object('result2') == Integer(1)
    assert scope.get_object('result') == Integer(2)