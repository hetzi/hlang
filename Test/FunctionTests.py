from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Integer, Boolean
from AST import DefaultScope

tests.clear()

@Test
def function_definition():
    tokens = Lexer('''

        func incr(x) {
            return x+1;
        }

        two = incr(1);

        decr = func(x) {
            return x-1;
        };

        one = decr(2);
    ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('two') == Integer(2)
    assert scope.get_object('one') == Integer(1)

@Test
def builtin_test():
    import math
    tokens = Lexer('''
        str = typeof("string");
        num = typeof(1);
        dec = typeof(1.0);
        sqrt2 = sqrt(2);
        a = null;
        a_is_null = isnull(a);
        is_null = isnull(null);
        b = 2;
        b_is_null = isnull(b);
        ''').tokenize()
    AST = Parser(tokens).parse()

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    assert scope.get_object('str').value == 'String'
    assert scope.get_object('num').value == 'Integer'
    assert scope.get_object('dec').value == 'Decimal'
    assert scope.get_object('sqrt2').value == math.sqrt(2)
    assert scope.get_object('a_is_null') == Boolean(True)
    assert scope.get_object('is_null') == Boolean(True)
    assert scope.get_object('b_is_null') == Boolean(False)