from Exception import ParseException
from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import Integer, Decimal
from AST import BlockNode, DefaultScope

tests.clear()


@Test
def integer_constants():
    tokens = Lexer('1; 2; 3; 4; -5;  -6 ;12391203; 12312; 0;').tokenize()
    AST = Parser(tokens).parse()

    assert type(AST) == BlockNode

    results = list(map(lambda v: Integer(v), [1, 2, 3, 4, -5, -6, 12391203, 12312, 0]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def integer_addition():
    tokens = Lexer('1+2+\n3 \t+4;1+10+12;    23+0;0+  \t0;').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Integer(v), [10, 23, 23, 0]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def invalid_integer_addition():
    exception = None
    try:
        Parser(Lexer('1+; +12').tokenize()).parse()
    except ParseException as e:
        exception = e

    assert type(exception) == ParseException


@Test
def integer_subtraction():
    tokens = Lexer('0-9;12-22;1-3-2;99-12;').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Integer(v), [-9, -10, -4, 87]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def invalid_integer_subtraction():
    exception = None
    try:
        Parser(Lexer('1-;').tokenize()).parse()
    except ParseException as e:
        exception = e

    assert type(exception) == ParseException


@Test
def integer_multiplication():
    tokens = Lexer('''
    _1 = 12*3; ;;
    _2 = 0*3; ;;;
;; _3 = 3*4*5;
_4 = 1*2*3*4;
''').tokenize()
    AST = Parser(tokens).parse()
    results = {
        '_1': Integer(36),
        '_2': Integer(0),
        '_3': Integer(60),
        '_4': Integer(24)
    }

    scope = DefaultScope()
    for node in AST.statements:
        node.evaluate(scope)

    for key, value in results.items():
        assert scope.get_object(key) == value

@Test
def integer_division():
    tokens = Lexer('4 / 2; 10 / 5; 2 / 4; 16 / 2 / 2; 8/8;').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Integer(v), [2, 2, 0, 4, 1]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def integer_modulo():
    tokens = Lexer('2 % 3; 4 % 5; 7 % 5;').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Integer(v), [2, 4, 2]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def integer_exponentiation():
    tokens = Lexer('2^3; 2^2^2; 0^2; 12^0;').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Integer(v), [8, 16, 0, 1]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def decimal_constants():
    tokens = Lexer('12.23; 3.9; 0.; .0; .123; 123.;').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Decimal(v), [12.23, 3.9, 0., 0., 0.123, 123.]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def compound_calculations():
    tokens = Lexer('-(-(-(-(-1)))); 2*3+1; 12*3^0; (1+1)^(2*3);').tokenize()
    AST = Parser(tokens).parse()
    results = list(map(lambda v: Integer(v), [-1, 7, 12, 64]))

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def decimal_additions():
    tokens = Lexer('1+1.5; 2.45+3.55\t\n;').tokenize()
    AST = Parser(tokens).parse()
    results = [Decimal(2.5), Decimal(6)]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def decimal_multiplications():
    tokens = Lexer('2*3.5; 7*1.5; 2.5*2.5;').tokenize()
    AST = Parser(tokens).parse()
    results = [Decimal(7.0), Decimal(10.5), Decimal(6.25)]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def decimal_exponentiations():
    tokens = Lexer('1.5^2; 2^0.5; 9.0^3;').tokenize()
    AST = Parser(tokens).parse()
    results = [Decimal(2.25), Decimal(2**0.5), Decimal(729.0)]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def unbalanced_parenthesis():
    exception = None
    try:
        Parser(Lexer('(1))').tokenize()).parse()
    except ParseException as e:
        exception = e

    assert type(exception) == ParseException
