from Exception import LexicalAnalysisException
from Lexer import Lexer, TokenType
from .TestDecorator import Test, tests

tests.clear()


@Test
def integers():
    tokens = Lexer('0 1 2 3 12 1231 239 3483 123').tokenize()
    should = [TokenType.INTEGER] * 9

    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def decimals():
    tokens = Lexer('.123 232.123 334. 0. .0 .123 0.12').tokenize()
    should = [TokenType.DECIMAL] * 7

    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def strings():
    tokens = Lexer(r'"string \" 1" "t /* */ sd" "//" "\""').tokenize()
    should = [TokenType.STRING] * 4

    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def invalid_strings():
    exception = None
    try:
        Lexer('" invalid string').tokenize()
    except LexicalAnalysisException as e:
        exception = e

    assert type(exception) == LexicalAnalysisException


@Test
def identifiers():
    tokens = Lexer('test _12 sd_2 _ __ asDsdf').tokenize()
    should = [TokenType.IDENTIFIER] * 6

    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def line_comments():
    tokens = Lexer('//\n    // test comment // lolo // sdfsdf\n').tokenize()
    assert len(tokens) == 0


@Test
def block_comments():
    tokens = Lexer('  /* not parsed /* //*/\n  ').tokenize()
    assert len(tokens) == 0


@Test
def operators():
    tokens = Lexer(' ^\n+\t-*/').tokenize()
    should = [TokenType.CIRCUMFLEX, TokenType.PLUS, TokenType.MINUS, TokenType.MULTIPLY, TokenType.DIVIDE]
    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def parenthesis():
    tokens = Lexer('  ( ) (( ) )').tokenize()
    should = [TokenType.OPEN_PARENTHESIS,
              TokenType.CLOSE_PARENTHESIS,
              TokenType.OPEN_PARENTHESIS,
              TokenType.OPEN_PARENTHESIS,
              TokenType.CLOSE_PARENTHESIS,
              TokenType.CLOSE_PARENTHESIS]

    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def curly_braces():
    tokens = Lexer(r' { { }} {').tokenize()
    should = [TokenType.OPEN_CURLY_BRACE,
              TokenType.OPEN_CURLY_BRACE,
              TokenType.CLOSE_CURLY_BRACE,
              TokenType.CLOSE_CURLY_BRACE,
              TokenType.OPEN_CURLY_BRACE]

    assert list(map(lambda t: t.tokenType, tokens)) == should


@Test
def brackets():
    tokens = Lexer('\n[\t ]][   \t\n][').tokenize()
    should = [TokenType.OPEN_BRACKET,
              TokenType.CLOSE_BRACKET,
              TokenType.CLOSE_BRACKET,
              TokenType.OPEN_BRACKET,
              TokenType.CLOSE_BRACKET,
              TokenType.OPEN_BRACKET]
    assert list(map(lambda t: t.tokenType, tokens)) == should

@Test
def keywords():
    tokens = Lexer('true false test if Return return').tokenize()
    should = [TokenType.KEYWORD,
              TokenType.KEYWORD,
              TokenType.IDENTIFIER,
              TokenType.KEYWORD,
              TokenType.IDENTIFIER,
              TokenType.KEYWORD]
    assert list(map(lambda t: t.tokenType, tokens)) == should