from Lexer import Lexer
from Parser import Parser
from .TestDecorator import Test, tests
from Type import String
from Exception import InvalidTypeException
from AST import DefaultScope

tests.clear()


@Test
def string_literals():
    tokens = Lexer('"test1"; "test2"; "last string";').tokenize()
    AST = Parser(tokens).parse()
    results = [String('test1'), String('test2'), String('last string')]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def string_addition():
    tokens = Lexer('"1" + "2" + "4"; "-1" + "" + "  ";').tokenize()
    AST = Parser(tokens).parse()
    results = [String('124'), String('-1  ')]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def string_multiplication():
    tokens = Lexer('"#" * 5; "_" * -5; "1" * 1; "test" * 0;').tokenize()
    AST = Parser(tokens).parse()
    results = [String('#####'), String(''), String('1'), String('')]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def string_concatenation():
    tokens = Lexer('"abc" + 12; 12.454 + "def";').tokenize()
    AST = Parser(tokens).parse()
    results = [String('abc12'), String('12.454def')]

    scope = DefaultScope()
    assert [node.evaluate(scope) for node in AST.statements] == results


@Test
def invalid_string_subtraction():
    exception = None
    try:
        AST = Parser(Lexer('"1" - "2";').tokenize()).parse()
        scope = DefaultScope()
        for node in AST.statements:
            node.evaluate(scope)
    except InvalidTypeException as e:
        exception = e

    assert type(exception) == InvalidTypeException

    exception = None
    try:
        AST = Parser(Lexer('1 - "2";').tokenize()).parse()
        scope = DefaultScope()
        for node in AST.statements:
            node.evaluate(scope)
    except InvalidTypeException as e:
        exception = e

    assert type(exception) == InvalidTypeException


@Test
def invalid_string_division():
    exception = None
    try:
        AST = Parser(Lexer('"1" / "2";').tokenize()).parse()
        scope = DefaultScope()
        for node in AST.statements:
            node.evaluate(scope)
    except InvalidTypeException as e:
        exception = e

    assert type(exception) == InvalidTypeException

    exception = None
    try:
        AST = Parser(Lexer('1 / "2";').tokenize()).parse()
        scope = DefaultScope()
        for node in AST.statements:
            node.evaluate(scope)
    except InvalidTypeException as e:
        exception = e

    assert type(exception) == InvalidTypeException
