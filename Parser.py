"""
<Program>    ::= { <Statement> }

<Statement>  ::= <Expression> ';'
               | <Assignment> ';'
               | <Function Definition>
               | <Return Statement>
               | <Block>
               | <While Loop>
               | <For Loop>
               | <If Statement>
               | 'break' ';'
               | ';'

<Block> ::= '{' { <Statement> } '}'

<While Loop> ::= 'while' <Expression> <Statement>

<For Loop> ::= 'for' ( <Assignment> | <Expression> )? ';' <Expression> ';' ( <Assignment> | <Expression> )? ';' <Statement>

<Block> ::= '{' { <Statement> } '}'

<If Statement> ::= 'if' <Expression> <Block> [ 'else' <Block> ]

<Return Statement> ::= 'return' <Expression> ';'
                     | 'return' ';'
                     | 'return' <Function Definition> ';'

<Assignment> ::= Identifier '=' <Expression>
               | Identifier '=' <Function Definition>

<Expression> ::= <And Expression> { '||' <And Expression> }

<And Expression> ::= <Bitwise OR> { '&&' <Bitwise OR> }

<Bitwise OR> ::= <Bitwise XOR> { '|' <Bitwise XOR> }

<Bitwise XOR> ::= <Bitwise AND> { '$' <Bitwise AND> }

<Bitwise AND> ::= <Equals Expression> { '&' <Equals Expression> }

<Equals Expression> ::= <Comparison> { ( '==' | '!=' ) <Comparison> }

<Comparison> ::= <Bitshift> { ( '>' | '>=' | '<' | '<=' ) <Bitshift> }

<Bitshift> ::= <Sum> { ( '>>' | '<<' ) <Sum> }

<Sum> ::= <Product> { ('+' | '-') <Product> }

<Product> ::= <Power> { ('*' | '/' | '%') <Power> }

<Power> ::= <Value> '^' <Power>
          | <Value>

<Value> ::= '-' <Value>
          | '~' <Value>
          | '!' <Value>
          | Identifier
          | Identifier '(' <Argument List> ')'
          | Integer
          | Decimal
          | '(' <Expression> ')'
          | '[' { <Expression } ']'
          | 'null'
          | 'true'
          | 'false'

<Argument List> ::= ( <Expression> { ',' <Expression> } )?

<Function Definition> ::= 'func' Identifier? '(' <Parameter List> ')' '{' { <Statement> }+ '}'

<Parameter List> ::= Identifier { ',' Identifier }
"""

from AST import *
from Type import Integer, Decimal, String, Boolean, Null
from Exception import ParseException
from Lexer import TokenType
from Operation.Binary import *
from Operation.Unary import *

class Marker:
    def __init__(self, index, token, loop_depth):
        self.index = index
        self.token = token
        self.loop_depth = loop_depth

class Parser:
    def __init__(self, tokens):
        self.tokens = tokens
        self.lookaheadIndex = 0
        self.lookahead = self.tokens[0]

        # keep track if we are in a loop
        self.loop_depth = 0
        self.markers = []

    def match(self, token_type=None):
        if token_type is not None and self.get_lookahead_token_type() != token_type:
            raise ParseException('Error: expected {}, got {}.'.format(token_type, self.get_lookahead_token_type()))

        # advance last marker if in backtracking mode
        if len(self.markers) > 0:
            next_index = self.markers[-1].index + 1
            if next_index >= len(self.tokens):
                self.markers[-1] = Marker(next_index, None, self.markers[-1].loop_depth)
                return
            self.markers[-1] = Marker(next_index, self.tokens[next_index], self.markers[-1].loop_depth)
        else:
            self.lookaheadIndex += 1
            if self.lookaheadIndex >= len(self.tokens):
                self.lookahead = None
                return

            self.lookahead = self.tokens[self.lookaheadIndex]

    def get_lookahead_token_type(self):

        # return token type of last marker if available
        if len(self.markers) > 0:
            if self.markers[-1] is not None:
                return self.markers[-1].token.tokenType
            else:
                return None

        if self.lookahead:
            return self.lookahead.tokenType
        return None

    def get_lookahead(self):
        # return the lookahead of the last marker if available
        if len(self.markers) > 0:
            return self.markers[-1].token

        return self.lookahead

    def get_loop_depth(self):
        if len(self.markers) > 0:
            return self.markers[-1].loop_depth

        return self.loop_depth

    def increment_loop_depth(self):
        if len(self.markers) > 0:
            self.markers[-1].loop_depth += 1
        else:
            self.loop_depth += 1

    def decrement_loop_depth(self):
        if len(self.markers) > 0:
            self.markers[-1].loop_depth -= 1
            if self.markers[-1].loop_depth < 0:
                raise ParseException('Loop depth cannot get smaller than zero.')
        else:
            self.loop_depth -= 1
            if self.loop_depth < 0:
                raise ParseException('Loop depth cannot get smaller than zero.')

    def set_marker(self):
        # if there was a previous marker start from that one
        if len(self.markers) > 0:
            self.markers.append(Marker(self.markers[-1].index, self.markers[-1].token, self.markers[-1].loop_depth))
        else:
            self.markers.append(Marker(self.lookaheadIndex, self.get_lookahead(), self.loop_depth))

    def reset_marker(self):
        self.markers.pop()

    def isEOF(self):
        return self.lookaheadIndex >= len(self.tokens)

    def parse(self):
        return self.Program()

    def speculate(self, rule):
        success = True
        self.set_marker()

        try:
            rule()
        except ParseException:
            success = False

        index = self.markers[-1].index
        self.reset_marker()
        return success, index

    def speculate_best_rule(self, rules):
        results = [(index, self.speculate(rule)) for index, rule in enumerate(rules)]

        # filter failed rules
        results = filter(lambda result: result[1][0], results)
        results = sorted(results, key=lambda result: result[1][1])
        if not results:
            return None

        return rules[results[-1][0]]

    def Program(self):
        """
        <Program> ::= { <Statement> }
        """

        nodes = []
        while not self.isEOF():
                result = self.Statement()
                if result:
                    nodes.append(result)
        return BlockNode(nodes)

    def Statement(self):
        """
        <Statement>  ::= <Expression> ';'
                       | <Assignment> ';'
                       | <Function Definition>
                       | <Return Statement>
                       | <Block>
                       | <If Statement>
                       | <While Loop>
                       | <For Loop>
                       | 'break' ';'
                       | ';'
        """
        if self.get_lookahead_token_type() == TokenType.SEMICOLON:
            self.match(TokenType.SEMICOLON)
            return BlockNode([])

        if self.get_lookahead_token_type() == TokenType.KEYWORD and self.get_lookahead().value == 'break':
            if self.get_loop_depth() == 0:
                raise ParseException('"break" statement found outside a loop.')

            self.match(TokenType.KEYWORD)
            self.match(TokenType.SEMICOLON)
            return BreakNode()


        best_rule = self.speculate_best_rule([self.Assignment,
                                              self.Expression,
                                              self.FunctionDefinition,
                                              self.ReturnStatement,
                                              self.Block,
                                              self.WhileLoop,
                                              self.IfStatement,
                                              self.ForLoop])

        if not best_rule:
            raise ParseException('Expected Expression or Assignment.')

        if best_rule == self.Assignment:
            assignment = self.Assignment()
            self.match(TokenType.SEMICOLON)
            return assignment

        if best_rule == self.Expression:
            expression = self.Expression()
            self.match(TokenType.SEMICOLON)
            return expression

        if best_rule == self.FunctionDefinition:
            return self.FunctionDefinition()

        if best_rule == self.ReturnStatement:
            return self.ReturnStatement()

        if best_rule == self.Block:
            return self.Block()

        if best_rule == self.WhileLoop:
            return self.WhileLoop()

        if best_rule == self.IfStatement:
            return self.IfStatement()

        if best_rule == self.ForLoop:
            return self.ForLoop()

    def Block(self):
        """
        <Block> ::= '{' { <Statement> } '}'
        """

        self.match(TokenType.OPEN_CURLY_BRACE)

        statements = []
        while self.speculate(self.Statement)[0]:
            statements.append(self.Statement())

        self.match(TokenType.CLOSE_CURLY_BRACE)

        return BlockNode(statements)

    def WhileLoop(self):
        """
        <While Loop> ::= 'while' <Expression> <Statement>
        """

        if self.get_lookahead_token_type() != TokenType.KEYWORD or self.get_lookahead().value != 'while':
            raise ParseException('Expected while.')

        self.match(TokenType.KEYWORD)

        expression = self.Expression()

        self.increment_loop_depth()
        statement = self.Statement()
        self.decrement_loop_depth()

        return WhileNode(expression, statement)

    def ForLoop(self):
        """
        <For Loop> ::= 'for' ( <Assignment> | <Expression> )? ';' <Expression> ';' ( <Assignment> | <Expression> )? ';' <Statement>
        """

        if self.get_lookahead_token_type() != TokenType.KEYWORD or self.get_lookahead().value != 'for':
            raise ParseException('Expected "for".')

        self.match(TokenType.KEYWORD)

        initialization_expression = None
        if self.get_lookahead_token_type() == TokenType.SEMICOLON:
            self.match(TokenType.SEMICOLON)
        else:
            best_rule = self.speculate_best_rule([self.Assignment, self.Expression])

            if not best_rule:
                raise ParseException('Expected Expression or Assignment.')

            initialization_expression = best_rule()

            self.match(TokenType.SEMICOLON)

        test_expression = self.Expression()
        self.match(TokenType.SEMICOLON)

        update_expression = None
        if self.get_lookahead_token_type() == TokenType.SEMICOLON:
            self.match(TokenType.SEMICOLON)
        else:
            best_rule = self.speculate_best_rule([self.Assignment, self.Expression])

            if not best_rule:
                raise ParseException('Expected Expression or Assignment.')

            update_expression = best_rule()

            self.match(TokenType.SEMICOLON)

        self.increment_loop_depth()
        body = self.Statement()
        self.decrement_loop_depth()

        return ForNode(initialization_expression, test_expression, update_expression, body)

    def IfStatement(self):
        """
        <If Statement> ::= 'if' <Expression> <Statement> { 'else' 'if' <Expression> <Statement> } [ 'else' <Statement' ]
        """

        if self.get_lookahead_token_type() != TokenType.KEYWORD or self.get_lookahead().value != 'if':
            raise ParseException('"if" expected.')

        self.match(TokenType.KEYWORD)

        conditions = [self.Expression()]
        consequences = [self.Statement()]
        otherwise = None

        while self.get_lookahead_token_type() == TokenType.KEYWORD and self.get_lookahead().value == 'else if':
            self.match(TokenType.KEYWORD)
            conditions.append(self.Expression())
            consequences.append(self.Statement())

        if self.get_lookahead_token_type() == TokenType.KEYWORD and self.get_lookahead().value == 'else':
            self.match(TokenType.KEYWORD)
            otherwise = self.Statement()

        return IfNode(conditions, consequences, otherwise)

    def ReturnStatement(self):
        """
        <Return Statement> ::= 'return' <Expression> ';'
                             | 'return' ';'
                             | 'return' <Function Definition> ';'
        """

        if self.get_lookahead_token_type() != TokenType.KEYWORD or self.get_lookahead().value != 'return':
            raise ParseException('Expected "return" keyword.')

        self.match(TokenType.KEYWORD)
        if self.get_lookahead_token_type() == TokenType.SEMICOLON:
            self.match(TokenType.SEMICOLON)
            return ReturnNode()

        best_rule = self.speculate_best_rule([self.Expression, self.FunctionDefinition])
        if not best_rule:
            raise ParseException('Expected ";", expression or function definition.')

        return ReturnNode(best_rule())

    def Expression(self):
        """
        <Expression> ::= <And Expression> { '||' <And Expression> }
        """

        lhs = self.AndExpression()
        while self.get_lookahead_token_type() == TokenType.LOGICAL_OR:
            self.match(TokenType.LOGICAL_OR)
            rhs = self.AndExpression()
            lhs = BinaryOperationNode(LogicalOr(), lhs, rhs)

        return lhs

    def AndExpression(self):
        """
        <And Expression> ::= <Bitwise OR> { '&&' <Bitwise OR> }
        """

        lhs = self.BitwiseOR()

        while self.get_lookahead_token_type() == TokenType.LOGICAL_AND:
            self.match(TokenType.LOGICAL_AND)
            rhs = self.BitwiseOR()
            lhs = BinaryOperationNode(LogicalAnd(), lhs, rhs)

        return lhs

    def BitwiseOR(self):
        """
        <Bitwise OR> ::= <Bitwise XOR> { '|' <Bitwise XOR> }
        """

        lhs = self.BitwiseXOR()

        while self.get_lookahead_token_type() == TokenType.BITWISE_OR:
            self.match(TokenType.BITWISE_OR)
            rhs = self.BitwiseXOR()
            lhs = BinaryOperationNode(OR(), lhs, rhs)

        return lhs

    def BitwiseXOR(self):
        """
        <Bitwise XOR> ::= <Bitwise AND> { '$' <Bitwise AND> }
        """

        lhs = self.BitwiseAND()

        while self.get_lookahead_token_type() == TokenType.BITWISE_XOR:
            self.match(TokenType.BITWISE_XOR)
            rhs = self.BitwiseAND()
            lhs = BinaryOperationNode(XOR(), lhs, rhs)

        return lhs

    def BitwiseAND(self):
        """
        <Bitwise AND> ::= <Equals Expression> { '&' <Equals Expression> }
        """

        lhs = self.EqualsExpression()
        while self.get_lookahead_token_type() == TokenType.BITWISE_AND:
            self.match(TokenType.BITWISE_AND)
            rhs = self.EqualsExpression()
            lhs = BinaryOperationNode(AND(), lhs, rhs)

        return lhs

    def EqualsExpression(self):
        """
        <Equals Expression> ::= <Comparison> { ( '==' | '!=' ) <Comparison> }
        """

        lhs = self.Comparison()

        while self.get_lookahead_token_type() in {TokenType.LOGICAL_EQUALITY, TokenType.LOGICAL_NOT_EQUALITY}:
            if self.get_lookahead_token_type() == TokenType.LOGICAL_EQUALITY:
                self.match(TokenType.LOGICAL_EQUALITY)
                lhs = BinaryOperationNode(Equality(), lhs, self.Comparison())
            elif self.get_lookahead_token_type() == TokenType.LOGICAL_NOT_EQUALITY:
                self.match(TokenType.LOGICAL_NOT_EQUALITY)
                lhs = UnaryOperationNode(LogicalNot(), BinaryOperationNode(Equality(), lhs, self.Comparison()))

        return lhs

    def Comparison(self):
        """
        <Comparison> ::= <Bitshift> { ( '>' | '>=' | '<' | '<=' ) <Bitshift> }
        """

        lhs = self.Bitshift()

        while self.get_lookahead_token_type() in {TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.SMALLER, TokenType.SMALLER_EQUAL}:
            if self.get_lookahead_token_type() == TokenType.GREATER:
                self.match(TokenType.GREATER)
                lhs = BinaryOperationNode(ComparisonGreater(), lhs, self.Bitshift())
            elif self.get_lookahead_token_type() == TokenType.GREATER_EQUAL:
                self.match(TokenType.GREATER_EQUAL)
                lhs = BinaryOperationNode(ComparisonGreaterEquals(), lhs, self.Bitshift())
            elif self.get_lookahead_token_type() == TokenType.SMALLER:
                self.match(TokenType.SMALLER)
                lhs = BinaryOperationNode(ComparisonSmaller(), lhs, self.Bitshift())
            elif self.get_lookahead_token_type() == TokenType.SMALLER_EQUAL:
                self.match(TokenType.SMALLER_EQUAL)
                lhs = BinaryOperationNode(ComparisonSmallerEquals(), lhs, self.Bitshift())

        return lhs

    def Bitshift(self):
        """
        <Bitshift> ::= <Sum> { ( '>>' | '<<' ) <Sum> }
        """

        lhs = self.Sum()

        while self.get_lookahead_token_type() in {TokenType.BITSHIFT_LEFT, TokenType.BITSHIFT_RIGHT}:
            if self.get_lookahead_token_type() == TokenType.BITSHIFT_LEFT:
                self.match(TokenType.BITSHIFT_LEFT)
                lhs = BinaryOperationNode(BitshiftLeft(), lhs, self.Sum())
            elif self.get_lookahead_token_type() == TokenType.BITSHIFT_RIGHT:
                self.match(TokenType.BITSHIFT_RIGHT)
                lhs = BinaryOperationNode(BitshiftRight(), lhs, self.Sum())

        return lhs

    def Sum(self):
        """
        <Sum> ::= <Product> { ('+' | '-') <Product> }
        """
        lhs = self.Product()
        while self.get_lookahead_token_type() in {TokenType.PLUS, TokenType.MINUS}:
            if self.get_lookahead_token_type() == TokenType.PLUS:
                self.match(TokenType.PLUS)
                rhs = self.Product()
                lhs = BinaryOperationNode(Addition(), lhs, rhs)
            elif self.get_lookahead_token_type() == TokenType.MINUS:
                self.match(TokenType.MINUS)
                rhs = self.Product()
                lhs = BinaryOperationNode(Subtraction(), lhs, rhs)

        return lhs

    def Product(self):
        """
        <Product> ::= <Power> { ('*' | '/' | '%') <Power> }
        """

        lhs = self.Power()

        while self.get_lookahead_token_type() in {TokenType.MULTIPLY, TokenType.DIVIDE, TokenType.PERCENT}:
            if self.get_lookahead_token_type() == TokenType.MULTIPLY:
                self.match(TokenType.MULTIPLY)
                rhs = self.Power()
                lhs = BinaryOperationNode(Multiplication(), lhs, rhs)
            elif self.get_lookahead_token_type() == TokenType.DIVIDE:
                self.match(TokenType.DIVIDE)
                rhs = self.Power()
                lhs = BinaryOperationNode(Division(), lhs, rhs)
            elif self.get_lookahead_token_type() == TokenType.PERCENT:
                self.match(TokenType.PERCENT)
                rhs = self.Power()
                lhs = BinaryOperationNode(Modulo(), lhs, rhs)

        return lhs

    def Power(self):
        """
        <Power> ::= <Value> '^' <Power>
                  | <Value>
        """

        lhs = self.Value()
        if self.get_lookahead_token_type() == TokenType.CIRCUMFLEX:
            self.match(TokenType.CIRCUMFLEX)
            rhs = self.Power()
            return BinaryOperationNode(Exponentiation(), lhs, rhs)
        else:
            return lhs

    def Value(self):
        """
        <Value> ::= '-' <Value>
                  | '~' <Value>
                  | '!' <Value>
                  | Identifier
                  | Identifier '(' <Argument List> ')'
                  | Integer
                  | Decimal
                  | '(' <Expression> ')'
                  | '[' { <Expression } ']' TODO
                  | 'null'
                  | 'true'
                  | 'false'
        """

        if self.get_lookahead_token_type() == TokenType.MINUS:
            self.match(TokenType.MINUS)
            return UnaryOperationNode(Negation(), self.Value())

        if self.get_lookahead_token_type() == TokenType.BITWISE_NOT:
            self.match(TokenType.BITWISE_NOT)
            return UnaryOperationNode(BitwiseNot(), self.Value())

        if self.get_lookahead_token_type() == TokenType.LOGICAL_NOT:
            self.match(TokenType.LOGICAL_NOT)
            return UnaryOperationNode(LogicalNot(), self.Value())

        if self.get_lookahead_token_type() == TokenType.KEYWORD and self.get_lookahead().value == 'null':
            self.match(TokenType.KEYWORD)
            return LiteralNode(Null())

        if self.get_lookahead_token_type() == TokenType.STRING:
            node = LiteralNode(String(
                self.lookahead.value[1:-1]  # used to remove the quotes
            ))
            self.match(TokenType.STRING)
            return node

        if self.get_lookahead_token_type() == TokenType.IDENTIFIER:
            name = self.get_lookahead().value
            self.match(TokenType.IDENTIFIER)
            if self.get_lookahead_token_type() == TokenType.OPEN_PARENTHESIS:
                self.match(TokenType.OPEN_PARENTHESIS)
                arg_list = self.ArgumentList()
                self.match(TokenType.CLOSE_PARENTHESIS)
                return FunctionCallNode(name, arg_list)
            else:
                return VariableNode(name)

        if self.get_lookahead_token_type() == TokenType.INTEGER:
            node = LiteralNode(Integer(self.get_lookahead().value))
            self.match(TokenType.INTEGER)
            return node

        if self.get_lookahead_token_type() == TokenType.DECIMAL:
            node = LiteralNode(Decimal(self.get_lookahead().value))
            self.match(TokenType.DECIMAL)
            return node

        if self.get_lookahead_token_type() == TokenType.OPEN_PARENTHESIS:
            self.match(TokenType.OPEN_PARENTHESIS)
            node = self.Expression()
            self.match(TokenType.CLOSE_PARENTHESIS)
            return node

        if self.get_lookahead_token_type() == TokenType.KEYWORD:
            if self.get_lookahead().value == 'true':
                self.match(TokenType.KEYWORD)
                return LiteralNode(Boolean(True))
            if self.get_lookahead().value == 'false':
                self.match(TokenType.KEYWORD)
                return LiteralNode(Boolean(False))

        raise ParseException('Expected identifier, function call, integer or expression.')

    def ArgumentList(self):
        """
        <Argument List> ::= ( <Expression> { ',' <Expression> } )?
        """
        args = []
        if self.speculate(self.Expression)[0]:
            args.append(self.Expression())
            while self.get_lookahead_token_type() == TokenType.COMMA:
                self.match(TokenType.COMMA)
                args.append(self.Expression())
        return args

    def Assignment(self):
        """
        <Assignment> ::= Identifier '=' <Expression>
                       | Identifier '=' <Function Definition>
        """

        name = self.get_lookahead().value
        self.match(TokenType.IDENTIFIER)
        self.match(TokenType.EQUALS)

        best_rule = self.speculate_best_rule([self.Expression, self.FunctionDefinition])

        if not best_rule:
            raise ParseException('Expected expression or function definition')

        if best_rule == self.Expression:
            return AssignmentNode(name, self.Expression())
        elif best_rule == self.FunctionDefinition:
            return AssignmentNode(name, self.FunctionDefinition())

    def FunctionDefinition(self):
        """
        <Function Definition> ::= 'func' Identifier? '(' <Parameter List> ')' '{' { <Statement> } '}'
        """
        if self.get_lookahead_token_type() != TokenType.KEYWORD or self.get_lookahead().value != 'func':
            raise ParseException('Expected "func" keyword.')
        self.match(TokenType.KEYWORD)

        function_name = None
        if self.get_lookahead_token_type() == TokenType.IDENTIFIER:
            function_name = self.get_lookahead().value
            self.match(TokenType.IDENTIFIER)
        self.match(TokenType.OPEN_PARENTHESIS)
        parameter_list = self.ParameterList()
        self.match(TokenType.CLOSE_PARENTHESIS)
        self.match(TokenType.OPEN_CURLY_BRACE)

        body = []
        while self.get_lookahead_token_type() != TokenType.CLOSE_CURLY_BRACE:
            result = self.Statement()
            if result:
                body.append(result)

        self.match(TokenType.CLOSE_CURLY_BRACE)
        return FunctionNode(function_name, parameter_list, BlockNode(body))

    def ParameterList(self):
        """
        <Parameter List> ::= ( Identifier { ',' Identifier } )?
        """
        names = []
        if self.get_lookahead_token_type() == TokenType.IDENTIFIER:
            names.append(self.get_lookahead().value)
            self.match(TokenType.IDENTIFIER)
            while self.get_lookahead_token_type() == TokenType.COMMA:
                self.match(TokenType.COMMA)
                names.append(self.get_lookahead().value)
                self.match(TokenType.IDENTIFIER)

        return names
