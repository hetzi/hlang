class Token:
    def __init__(self, tokenType, value):
        self.tokenType = tokenType
        self.value = value

    def __str__(self):
        return '{} {}'.format(self.tokenType, self.value)
