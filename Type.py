from Exception import InvalidTypeException

class Null:
    type_index = 5

    def convert(self, result_type):
        if result_type == Null:
            return self
        else:
            raise InvalidTypeException('Null cannot be casted into {}'.format(result_type))

    def __str__(self):
        return 'null'

    def __eq__(self, other):
        return type(other) == Null

    def __ne__(self, other):
        return not self.__eq__(other)

class Decimal:
    type_index = 3

    def __init__(self, value):
        self.value = float(value)

    def convert(self, result_type):
        if result_type == Decimal:
            return self
        elif result_type == Integer:
            return Integer(self.value)
        elif result_type == String:
            return String(self.value)
        else:
            raise InvalidTypeException('Decimal cannot be converted into {}.'.format(result_type))

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

class Boolean:
    type_index = 1

    def __init__(self, value):
        self.value = bool(value)

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

class Integer:
    type_index = 2

    def __init__(self, value):
        self.value = int(value)

    def convert(self, result_type):
        if result_type == Integer:
            return self
        elif result_type == Boolean:
            return Boolean(self.value)
        elif result_type == Decimal:
            return Decimal(self.value)
        elif result_type == String:
            return String(self.value)
        else:
            raise InvalidTypeException('Integer cannot be converted into {}'.format(result_type))

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

class String:
    type_index = 4

    def __init__(self, value):
        self.value = str(value)

    def convert(self, result_type):
        if result_type == String:
            return self
        elif result_type == Integer:
            return Integer(self)
        elif result_type == Decimal:
            return Decimal(self)
        else:
            raise InvalidTypeException('String cannot be converted into {}'.format(result_type))

    def __str__(self):
        return '{}'.format(self.value)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

class Variable:
    def __init__(self, name, value=None):
        self.name = name
        self.value = value

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

class Array:

    type_index = 6

    def __init__(self, content):
        if len(content) > 0:
            self.content = content
        else:
            self.content = []
