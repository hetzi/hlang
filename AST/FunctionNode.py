class FunctionNode:
    def __init__(self, name, parameter_list, body):
        self.name = name
        self.parameter_list = parameter_list
        self.body = body

    def evaluate(self, scope):
        scope.set_object(self.name, self)
        return self

    def __str__(self):
        import re
        return 'func {}({}) {{\n{}}}'.format(self.name if self.name else '',
                                             ', '.join(self.parameter_list),
                                             re.sub(r'^(.+)', r'\t\1', str(self.body), flags=re.MULTILINE))
