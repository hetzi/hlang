class LiteralNode:
    def __init__(self, value):
        self.value = value

    def evaluate(self, scope):
        return self.value

    def __str__(self):
        return str(self.value)
