from Exception import BreakException

class BreakNode:
    def evaluate(self, scope):
        raise BreakException()