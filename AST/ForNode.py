from Type import Boolean
from Exception import BreakException
from AST import Scope

class ForNode:
    def __init__(self, initialization_expression, test_expression, update_expression, body):
        self.initialization_expression = initialization_expression
        self.test_expression = test_expression
        self.update_expression = update_expression
        self.body = body

    def evaluate(self, scope):

        child_scope = Scope()
        child_scope = child_scope.merge(scope)

        if self.initialization_expression:
            self.initialization_expression.evaluate(child_scope)

        if self.test_expression:
            test_func = lambda: self.test_expression.evaluate(child_scope) == Boolean(True)
        else:
            test_func = lambda: True

        try:
            while test_func():
                self.body.evaluate(child_scope)
                if self.update_expression:
                    self.update_expression.evaluate(child_scope)
        except BreakException:
            scope.update(child_scope)
            return

        scope.update(child_scope)