from Exception import ArityException, FunctionReturnException, InvalidTypeException
from AST.Scope import Scope
from AST import FunctionNode
from Type import Null
from Builtins import BuiltinFunction

class FunctionCallNode:
    def __init__(self, name, argument_list):
        self.name = name
        self.argument_list = argument_list

    def evaluate(self, scope):
        # get function AST
        func = scope.get_object(self.name)
        if not isinstance(func, FunctionNode):
            raise InvalidTypeException('Object {} is not a function.'.format(func))

        # compare number of arguments
        if len(func.parameter_list) != len(self.argument_list):
            raise ArityException('Function {}: argument list does not match arity of {}.'.format(
                self.name,
                len(self.argument_list)))

        func_scope = Scope()
        # set the argument names in the scope
        for i in range(len(self.argument_list)):
            func_scope.set_object(func.parameter_list[i], self.argument_list[i].evaluate(scope))

        func_scope = scope.merge(func_scope)

        # call the function and await a return value
        ret_value = Null()
        try:
            if type(func) == BuiltinFunction:
                func.evaluate(func_scope)
            else:
                for statement in func.body.statements:
                    statement.evaluate(func_scope)
        except FunctionReturnException as e:
            ret_value = e.return_value

        # update the altered objects in the outer scope
        scope.update(func_scope)

        return ret_value

    def __str__(self):
        return '{}({})'.format(self.name, ', '.join([str(arg) for arg in self.argument_list]))
