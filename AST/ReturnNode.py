from Exception import FunctionReturnException

class ReturnNode:
    def __init__(self, return_node=None):
        self.return_node = return_node

    def evaluate(self, scope):
        value = self.return_node.evaluate(scope)
        raise FunctionReturnException(value)

    def __str__(self):
        return 'return {}'.format(self.return_node)