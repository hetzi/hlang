from Type import Boolean
from Exception import BreakException

class WhileNode:
    def __init__(self, condition, body):
        self.condition = condition
        self.body = body

    def evaluate(self, scope):
        try:
            while self.condition.evaluate(scope) == Boolean(True):
                self.body.evaluate(scope)
        except BreakException:
            return

    def __str__(self):
        return 'while {} {}'.format(self.condition, self.body)