class AssignmentNode:
    def __init__(self, variable_name, rhs):
        self.variable_name = variable_name
        self.rhs = rhs

    def evaluate(self, scope):
        value = self.rhs.evaluate(scope)
        scope.set_object(self.variable_name, value)

    def __str__(self):
        return '{} = {}'.format(self.variable_name, self.rhs)