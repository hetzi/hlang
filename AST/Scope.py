from Exception import NotDefinedException, FunctionReturnException, InvalidTypeException
from Builtins import BuiltinFunction
from Type import Decimal, String, Integer, Null, Boolean
import math
import sys

class Scope:
    def __init__(self):
        self.objects = dict()

    def get_object(self, name):
        if not name in self.objects:
            raise NotDefinedException('Object {} is not defined in the current scope.'.format(name))

        return self.objects[name]

    def set_object(self, name, value):
        self.objects[name] = value

    def merge(self, other_scope):
        merged_objects = self.objects.copy()

        merged_objects.update(other_scope.objects)

        scope = Scope()
        scope.objects = merged_objects
        return scope

    def update(self, inner):
        for obj_name, value in inner.objects.items():
            if obj_name in self.objects:
                self.objects[obj_name] = value

class DefaultScope(Scope):
    def __init__(self):
        super().__init__()

        # Functions
        self.objects['print']   = BuiltinFunction('print', 1, lambda scope: print(scope.get_object('arg0'), end=''))
        self.objects['println'] = BuiltinFunction('println', 1, lambda scope: print(scope.get_object('arg0')))
        self.objects['read']    = BuiltinFunction('read', 0, lambda scope: self.raise_helper(FunctionReturnException(String(sys.stdin.read(1)))))
        self.objects['readln']  = BuiltinFunction('readln', 0, lambda scope: self.raise_helper(FunctionReturnException(input())))
        self.objects['typeof']  = BuiltinFunction('typeof', 1, lambda scope: self.raise_helper(FunctionReturnException(String(scope.get_object('arg0').__class__.__name__))))
        self.objects['sqrt']    = BuiltinFunction('sqrt', 1, lambda scope: self.raise_helper(FunctionReturnException(Decimal(self.sqrt(scope.get_object('arg0'))))))
        self.objects['isnull']  = BuiltinFunction('isnull', 1, lambda scope: self.raise_helper(FunctionReturnException(Boolean(type(scope.get_object('arg0')) == Null))))

        # Variables
        self.objects['PI']      = Decimal(math.pi)
        self.objects['E']       = Decimal(math.e)

    @staticmethod
    def raise_helper(x):
        raise x

    @staticmethod
    def sqrt(x):
        if not (type(x) == Integer or type(x) == Decimal):
            raise InvalidTypeException('Type {} not supported for sqrt.'.format(type(x)))

        return math.sqrt(float(x.value))

