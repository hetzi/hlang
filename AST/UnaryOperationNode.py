class UnaryOperationNode:
    def __init__(self, unary_operation, node):
        self.unary_operation = unary_operation
        self.node = node

    def evaluate(self, scope):
        return self.unary_operation.evaluate(self.node.evaluate(scope))

    def __str__(self):
        return '({} {})'.format(self.unary_operation, self.node)