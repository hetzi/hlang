from AST.Scope import Scope
from Exception import BreakException

class BlockNode:
    def __init__(self, statements):
        self.statements = statements

    def evaluate(self, scope):
        child_scope = Scope()
        child_scope = child_scope.merge(scope)
        for node in self.statements:
            try:
                node.evaluate(child_scope)
            except BreakException as e:
                scope.update(child_scope)
                raise e

        # update the altered objects in the outer scope
        scope.update(child_scope)

    def __str__(self):
        return ';\n'.join([str(s) for s in self.statements]) + ';\n'
