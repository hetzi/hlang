from Type import Boolean

class IfNode:
    def __init__(self, conditions, consequences, otherwise=None):
        self.conditions = conditions
        self.consequences = consequences
        self.otherwise = otherwise

    def evaluate(self, scope):

        matched = False
        for index, condition in enumerate(self.conditions):
            if condition.evaluate(scope) == Boolean(True):
                self.consequences[index].evaluate(scope)
                matched = True
                break

        if not matched and self.otherwise:
            self.otherwise.evaluate(scope)

    def __str__(self):
        return 'if {} {{ {} }}{}'.format(self.conditions, self.consequences, ' else {{ {} }}'.format(self.otherwise) if self.otherwise else '')