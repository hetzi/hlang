class VariableNode:
    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        return scope.get_object(self.name)

    def __str__(self):
        return str(self.name)