class BinaryOperationNode:
    def __init__(self, binaryOperation, lhs, rhs):
        self.binaryOperation = binaryOperation
        self.lhs = lhs
        self.rhs = rhs

    def evaluate(self, scope):
        return self.binaryOperation.evaluate(self.lhs.evaluate(scope), self.rhs.evaluate(scope))

    def __str__(self):
        return '({} {} {})'.format(
            self.lhs,
            self.binaryOperation,
            self.rhs
        )
