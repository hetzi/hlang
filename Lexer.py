import re
from enum import Enum
from Exception import LexicalAnalysisException
from Token import Token

class TokenType(Enum):
    IDENTIFIER           = 1
    INTEGER              = 2
    DECIMAL              = 3
    COMMA                = 4
    PLUS                 = 5
    MINUS                = 6
    DIVIDE               = 7
    MULTIPLY             = 8
    PERCENT              = 9
    EQUALS               = 10
    OPEN_PARENTHESIS     = 11
    CLOSE_PARENTHESIS    = 12
    OPEN_CURLY_BRACE     = 13
    CLOSE_CURLY_BRACE    = 14
    OPEN_BRACKET         = 15
    CLOSE_BRACKET        = 16
    STRING               = 17
    CIRCUMFLEX           = 18
    SEMICOLON            = 19
    KEYWORD              = 20
    BITWISE_OR           = 21
    BITWISE_AND          = 22
    BITWISE_XOR          = 23
    BITWISE_NOT          = 24
    LOGICAL_NOT          = 25
    LOGICAL_AND          = 26
    LOGICAL_OR           = 27
    LOGICAL_EQUALITY     = 28
    LOGICAL_NOT_EQUALITY = 29
    GREATER              = 30
    GREATER_EQUAL        = 31
    SMALLER              = 32
    SMALLER_EQUAL        = 33
    BITSHIFT_RIGHT       = 34
    BITSHIFT_LEFT        = 35

class Lexer:
    def __init__(self, code):

        self.keywords = [
            'func', 'if', 'else', 'while', 'for', 'return', 'true', 'false', 'null', 'break'
        ]

        self.regex_list = [
            (TokenType.KEYWORD,              re.compile('|'.join(self.keywords))),
            (TokenType.IDENTIFIER,           re.compile(r'[a-zA-Z_][a-zA-Z_0-9]*')),
            (TokenType.DECIMAL,              re.compile(r'((0|[1-9]\d*)?\.\d+)|((0|[1-9]\d*)\.\d*)')),
            (TokenType.INTEGER,              re.compile(r'0|([1-9][0-9]*)')),
            (TokenType.COMMA,                re.compile(r',')),
            (TokenType.PLUS,                 re.compile(r'\+')),
            (TokenType.MINUS,                re.compile(r'-')),
            (TokenType.DIVIDE,               re.compile(r'/')),
            (TokenType.MULTIPLY,             re.compile(r'\*')),
            (TokenType.PERCENT,              re.compile(r'%')),
            (TokenType.EQUALS,               re.compile(r'=')),
            (TokenType.OPEN_PARENTHESIS,     re.compile(r'\(')),
            (TokenType.CLOSE_PARENTHESIS,    re.compile(r'\)')),
            (TokenType.OPEN_CURLY_BRACE,     re.compile(r'{')),
            (TokenType.CLOSE_CURLY_BRACE,    re.compile(r'}')),
            (TokenType.OPEN_BRACKET,         re.compile(r'\[')),
            (TokenType.CLOSE_BRACKET,        re.compile(r'\]')),
            (TokenType.STRING,               re.compile(r'".*?(?<!\\)"')),
            (TokenType.CIRCUMFLEX,           re.compile(r'\^')),
            (TokenType.SEMICOLON,            re.compile(r';')),
            (TokenType.BITWISE_OR,           re.compile(r'\|')),
            (TokenType.BITWISE_AND,          re.compile(r'&')),
            (TokenType.BITWISE_XOR,          re.compile(r'\$')),
            (TokenType.BITWISE_NOT,          re.compile(r'~')),
            (TokenType.LOGICAL_NOT,          re.compile(r'!')),
            (TokenType.LOGICAL_AND,          re.compile(r'&&')),
            (TokenType.LOGICAL_OR,           re.compile(r'\|\|')),
            (TokenType.LOGICAL_EQUALITY,     re.compile(r'==')),
            (TokenType.LOGICAL_NOT_EQUALITY, re.compile(r'!=')),
            (TokenType.GREATER,              re.compile(r'>')),
            (TokenType.GREATER_EQUAL,        re.compile(r'>=')),
            (TokenType.SMALLER,              re.compile(r'<')),
            (TokenType.SMALLER_EQUAL,        re.compile(r'<=')),
            (TokenType.BITSHIFT_RIGHT,       re.compile(r'>>')),
            (TokenType.BITSHIFT_LEFT,        re.compile(r'<<'))
        ]

        self.code = code
        self.regex_whitespace = re.compile(r'^\s+')
        self.regex_line_comment = re.compile(r'^//.*(?=\n)')
        self.regex_block_comment = re.compile(r'^/\*(.|\n)*?\*/')

    def tokenize(self):
        # track the current line and position on the line
        line = 1
        position = 0
        tokens = []

        # try to match a token while there is code to consume
        while self.code:

            # skip whitespace and comments until there is a new token available
            processed_code = self.code
            while True:
                start_length = len(processed_code)

                processed_code = self.regex_whitespace.sub('', processed_code)
                processed_code = self.regex_line_comment.sub('', processed_code)
                processed_code = self.regex_block_comment.sub('', processed_code)

                if len(processed_code) == start_length:
                    break

            changed_lines = self.code.count('\n') - processed_code.count('\n')
            if changed_lines != 0:
                line += changed_lines
                position = 0

            self.code = processed_code

            if not self.code:
                break

            # keep track of the longest possible match
            longest_match = ''
            token = None

            # try every token if we can make a longer match
            for tokenType, regex in self.regex_list:
                match = regex.match(self.code)

                if match and len(match.group()) > len(longest_match):
                    longest_match = match.group()
                    token = tokenType

            # no token was found -> print error and exit program
            if not longest_match:
                raise (LexicalAnalysisException('No valid token found at line {} position {}'.format(line, position)))

            position += len(longest_match)

            # truncate code
            self.code = self.code[len(longest_match):]

            tokens.append(Token(token, longest_match))

        # if a comment was in the last line
        # if tokens[-1].tokenType != TokenType.NEWLINE:
        #		tokens.append(Token(TokenType.NEWLINE, None))

        # merge consecutive if and else

        merged = []
        i = 0
        while i < len(tokens) - 1:
            if tokens[i].tokenType == TokenType.KEYWORD and tokens[i].value == 'else' and tokens[i + 1].tokenType == TokenType.KEYWORD and tokens[i + 1].value == 'if':
                merged.append(Token(TokenType.KEYWORD, 'else if'))
                i += 1
            else:
                merged.append(tokens[i])

            i += 1


        if len(merged) > 0 and merged[-1].value != 'else if':
            merged.append(tokens[-1])

        return merged
