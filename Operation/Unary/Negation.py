from Type import Integer, Decimal
from Exception import InvalidTypeException

class Negation:
    def evaluate(self, argument):
        if type(argument) not in {Integer, Decimal}:
            raise InvalidTypeException('Negation only applicable to integer and decimal values ({} given).'.format(argument))

        return type(argument)(-argument.value)

    def __str__(self):
        return '-'