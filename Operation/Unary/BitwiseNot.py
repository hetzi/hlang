from Exception import InvalidTypeException
from Type import Integer

class BitwiseNot:
    def evaluate(self, argument):
        if type(argument) != Integer:
            raise InvalidTypeException('BitwiseNot is only applicable to integer values ({} given).'.format(argument))

        return Integer(~argument.value)

    def __str__(self):
        return '~'
