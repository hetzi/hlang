from Exception import InvalidTypeException
from Type import Boolean

class LogicalNot:
    def evaluate(self, argument):
        if type(argument) != Boolean:
            raise InvalidTypeException('LogicalNot is only applicable to boolean values ({} given).'.format(argument))

        return Boolean(not argument.value)

    def __str__(self):
        return '!'
