from Operation import TypePromotion
from Type import String, Integer
from Exception import InvalidTypeException

class Division:
    def evaluate(self, lhs, rhs):
        promoted_type = TypePromotion.get_type(lhs.type_index, rhs.type_index)

        if promoted_type == String:
            raise InvalidTypeException('Type String is not supported for division.')

        lhs = lhs.convert(promoted_type)
        rhs = rhs.convert(promoted_type)

        if promoted_type == Integer:
            return promoted_type(lhs.value // rhs.value)

        return promoted_type(lhs.value / rhs.value)

    def __str__(self):
        return '/'
