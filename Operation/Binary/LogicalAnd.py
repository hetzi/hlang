from Type import Boolean
from Exception import InvalidTypeException

class LogicalAnd:
    def evaluate(self, lhs, rhs):
        if type(lhs) != Boolean or type(rhs) != Boolean:
            raise InvalidTypeException('Logical and is only applicable to boolean values ({}, {} given).'.format(lhs, rhs))

        return Boolean(lhs.value and rhs.value)

    def __str__(self):
        return '&&'
