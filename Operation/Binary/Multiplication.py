from Operation import TypePromotion
from Type import Integer, String

class Multiplication:
    def evaluate(self, lhs, rhs):

        if {type(lhs), type(rhs)} == {Integer, String}:
            return String(lhs.value * rhs.value)

        promoted_type = TypePromotion.get_type(lhs.type_index, rhs.type_index)

        lhs = lhs.convert(promoted_type)
        rhs = rhs.convert(promoted_type)

        return promoted_type(lhs.value * rhs.value)

    def __str__(self):
        return '*'
