from Type import Boolean

class Equality:

    def evaluate(self, lhs, rhs):
        if type(lhs) != type(rhs):
            return Boolean(False)

        return Boolean(lhs.value == rhs.value)

    def __str__(self):
        return '=='