from Type import Decimal, Integer, Boolean
from Exception import InvalidTypeException

def check_types(operator_name, lhs, rhs):
    if type(lhs) not in {Integer, Decimal} or type(rhs) not in {Integer, Decimal}:
        raise InvalidTypeException('Operator "{}" is only applicable to Integer and Decimal types ({}, {} given)'.format(operator_name, lhs, rhs))

    if {type(lhs), type(rhs)} == {Integer, Decimal}:
        lhs = lhs.convert(Decimal)
        rhs = rhs.convert(Decimal)

    return lhs, rhs

class ComparisonGreater:
    def evaluate(self, lhs, rhs):
        lhs, rhs = check_types('>', lhs, rhs)
        return Boolean(lhs.value > rhs.value)

    def __str__(self):
        return '>'

class ComparisonGreaterEquals:
    def evaluate(self, lhs, rhs):
        lhs, rhs = check_types('>=', lhs, rhs)
        return Boolean(lhs.value >= rhs.value)

    def __str__(self):
        return '>='

class ComparisonSmaller:
    def evaluate(self, lhs, rhs):
        lhs, rhs = check_types('<', lhs, rhs)
        return Boolean(lhs.value < rhs.value)

    def __str__(self):
        return '<'

class ComparisonSmallerEquals:
    def evaluate(self, lhs, rhs):
        lhs, rhs = check_types('<=', lhs, rhs)
        return Boolean(lhs.value <= rhs.value)

    def __str__(self):
        return '<='


