from Operation import TypePromotion
from Type import String
from Exception import InvalidTypeException

class Exponentiation:
    def evaluate(self, lhs, rhs):
        promoted_type = TypePromotion.get_type(lhs.type_index, rhs.type_index)

        if promoted_type == String:
            raise InvalidTypeException('Type String is not supported for exponentiation.')

        return promoted_type(lhs.value ** rhs.value)

    def __str__(self):
        return '^'
