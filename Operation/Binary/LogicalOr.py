from Type import Boolean
from Exception import InvalidTypeException

class LogicalOr:
    def evaluate(self, lhs, rhs):
        if type(lhs) != Boolean or type(rhs) != Boolean:
            raise InvalidTypeException('Logical or is only applicable to boolean values ({}, {} given).'.format(lhs, rhs))

        return Boolean(lhs.value or rhs.value)

    def __str__(self):
        return '||'
