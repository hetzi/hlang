from Operation import TypePromotion

class Addition:
    def evaluate(self, lhs, rhs):
        promoted_type = TypePromotion.get_type(lhs.type_index, rhs.type_index)

        lhs = lhs.convert(promoted_type)
        rhs = rhs.convert(promoted_type)

        return promoted_type(lhs.value + rhs.value)

    def __str__(self):
        return '+'
