from Exception import InvalidTypeException
from Type import Integer

def check_types(op_name, value, places):
    if type(value) != Integer or type(places) != Integer:
        raise InvalidTypeException('Operator "{}" is only applicable to integer values ({}, {} given).'.format(op_name, value, places))

class BitshiftRight:
    def evaluate(self, argument, places):
        check_types('>>', argument, places)

        return Integer(argument.value >> places.value)

    def __str__(self):
        return '>>'

class BitshiftLeft:
    def evaluate(self, argument, places):
        check_types('<<', argument, places)

        return Integer(argument.value << places.value)
