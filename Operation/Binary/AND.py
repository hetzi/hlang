from Type import Integer
from Exception import InvalidTypeException

class AND:

    def evaluate(self, lhs, rhs):
        if type(lhs) != Integer or type(rhs) != Integer:
            raise InvalidTypeException('AND is only applicable to integer values ({}, {} given).'.format(lhs, rhs))

        return Integer(lhs.value & rhs.value)

    def __str__(self):
        return '&'