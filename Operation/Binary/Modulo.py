from Type import Integer
from Exception import InvalidTypeException

class Modulo:
    def evaluate(self, lhs, rhs):
        if not (type(lhs) == Integer and type(rhs) == Integer):
            raise InvalidTypeException(
                'Modulo is only valid for integers. Given types are: {} {}.'.format(
                    type(lhs),
                    type(rhs))
            )

        if rhs.value == 0:
            raise ZeroDivisionError('Modulo by zero is not allowed.')

        return Integer(lhs.value % rhs.value)

    def __str__(self):
        return '%'
