from Type import Integer, Decimal, String


class TypePromotion:
    """
    Calculates the resulting type of an arithmetic binary operator
    """

    result_type = [
       # class Boolean  Integer  Decimal  String   Null  Array
        [None, None,    None,     None,    None,   None, None], # class
        [None, None,    None,     None,    String, None, None], # Boolean
        [None, None,    Integer,  Decimal, String, None, None], # Integer
        [None, None,    Decimal,  Decimal, String, None, None], # Decimal
        [None, String,  String,   String,  String, None, None], # String
        [None, None,    None,     None,    None,   None, None], # Null
        [None, None,    None,     None,    None,   None, None]  # Array
    ]

    @staticmethod
    def get_type(type_index_lhs, type_index_rhs):
        return TypePromotion.result_type[type_index_lhs][type_index_rhs]
