class FunctionReturnException(Exception):
    def __init__(self, return_value):
        self.return_value = return_value