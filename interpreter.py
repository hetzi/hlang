#!/bin/python3

"""
TODO:
- improve scoping
- add break and continue
- add list
"""

import os.path
import sys

from Lexer import Lexer
from Parser import Parser

def main():
    if len(sys.argv) < 2:
        print('You must specify the source file.')
        sys.exit(-1)
    path = sys.argv[1]
    if not os.path.isfile(path):
        print('{} is not a file.'.format(path))
        sys.exit(-1)

    with open(path, 'r') as file:
        code = file.read()

    lexer = Lexer(code)
    tokens = lexer.tokenize()
    print('\n'.join(['{} {}'.format(index, token) for index, token in enumerate(tokens)]))

    parser = Parser(tokens)
    AST = parser.parse()

    print('\n'.join([str(index) + ' ' + str(node) for index, node in enumerate(AST)]))
    print()
    print('\n'.join([str(node.evaluate()) for node in AST]))

if __name__ == '__main__':
    main()
