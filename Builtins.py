from AST import FunctionNode

class BuiltinFunction(FunctionNode):
    def __init__(self, name, parameters, body):
        super().__init__(name, ['arg{}'.format(i) for i in range(0, parameters)], None)
        self.body = body

    def evaluate(self, scope):
        self.body(scope)

