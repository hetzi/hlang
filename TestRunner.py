END = '\033[0m'
GREEN = '\033[92m'
RED = '\033[91m'

overall_tests = 0
overall_passed = 0

def try_function(func):
    try:
        func()
    except AssertionError:
        return False
    return True

def run_suite(tests, suite_name):
    message = 'Running {} ({})'.format(suite_name, len(tests))
    print(message)
    print('=' * len(message))
    print()

    longest_method_name = max(map(lambda f: len(f.__name__), tests))
    longest_test_number = len(str(len(tests)))

    passed = 0
    for index, func in enumerate(tests):
        test_number = ' ' * (longest_test_number - len(str((index + 1)))) + str(index + 1)
        print('Running Test {} ({})'.format(test_number, func.__name__), end='')
        result = try_function(func)
        if result:
            passed += 1
            print(GREEN + ' ' * (longest_method_name - len(func.__name__)) + ' SUCCEEDED' + END)
        else:
            print(RED + ' ' * (longest_method_name - len(func.__name__)) + ' FAILED' + END)

    print()
    print('=' * len(message))

    print('{}/{} Tests passed ({}%)'.format(passed, len(tests), round(100 * passed / len(tests), 2)))
    print()
    return passed

def print_summary():
    print('Executed {} tests'.format(overall_tests))
    print('{} tests passed'.format(overall_passed))
    print('{} tests failed'.format(overall_tests - overall_passed))
    print('Success: {}%'.format(round(100 * overall_passed / overall_tests, 2)))

def execute_test(tests, name):
    passed = run_suite(tests, name)

    global overall_passed, overall_tests
    overall_passed += passed
    overall_tests += len(tests)

def run_tests():

    from Test.TokenizerTests import tests as tokenizer_tests
    execute_test(tokenizer_tests, 'Tokenizer Tests')

    from Test.ArithmeticTests import tests as arithmetic_tests
    execute_test(arithmetic_tests, 'Arithmetic Tests')

    from Test.StringTests import tests as string_tests
    execute_test(string_tests, 'String Tests')

    from Test.ParseTests import tests as parse_tests
    execute_test(parse_tests, 'Parse Tests')

    from Test.BinaryTests import tests as binary_tests
    execute_test(binary_tests, 'Binary Tests')

    from Test.WhileTests import tests as while_tests
    execute_test(while_tests, 'While Tests')

    from Test.ForTests import tests as for_tests
    execute_test(for_tests, 'For Tests')

    from Test.ScopingTests import tests as scoping_tests
    execute_test(scoping_tests, 'Scoping Tests')

    from Test.IfTests import tests as if_tests
    execute_test(if_tests, 'If Tests')

    from Test.FunctionTests import tests as function_tests
    execute_test(function_tests, 'Function Tests')

    from Test.BreakTest import tests as break_tests
    execute_test(break_tests, 'Break Tests')

    print_summary()

if __name__ == '__main__':
    run_tests()
